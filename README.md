# Exercise 2.8 Loop Detection

Loop Detection: Given a circular linked list, implement an algorithm that returns the node at the
beginning of the loop.

DEFINITION

Circular linked list: A (corrupt) linked list in which a node's next pointer points to an earlier node, so as to make a loop in the linked list.

EXAMPLE

Input: A -> B -> C -> D -> E -> C [the same C as earlier]

Output: C

_Hints_: #50, #69, #83, #90

Whiteboard [analysis](whiteboard-images/analysis.png), [test cases](whiteboard-images/test-cases.png) and [code](whiteboard-images/code.png) are available using the links.

Questions, issues, comments, PRs all welcome.
