import com.pajato.ctci.Node
import com.pajato.ctci.createLinkedList
import kotlin.test.Test
import kotlin.test.assertEquals

class Test {

    @Test fun `verify that an empty list returns null`() {
        assertEquals(null, detectLoop<Int>(null))
    }

    @Test fun `verify that an single node returns null`() {
        assertEquals(null, detectLoop(Node(1)))
    }

    @Test fun `verify that a non-looping list returns null`() {
        assertEquals(null, detectLoop(createLinkedList(1, 2, 3, 4)))
    }

    @Test fun `verify that a runway of 0 returns the first (head) node`() {
        val loopedList = createLinkedList(1, 2, 3, 4, 5, 6, 7, 8)
        val nine = Node(9)
        nine.joinToEndOf(loopedList!!)
        nine.next = loopedList

        assertEquals(loopedList, detectLoop(loopedList))
    }

    @Test fun `verify that a runway of 1 returns the second node`() {
        val loopedList = createLinkedList(2, 3, 4, 5, 6, 7, 8)
        val head = createLinkedList(1)
        head!!.next = loopedList
        val nine = Node(9)
        nine.joinToEndOf(loopedList!!)
        nine.next = loopedList
        val actual = detectLoop(head)
        assertEquals(loopedList, actual)
    }

    @Test fun `verify that a runway of 2 returns the third node`() {
        val loopedList = createLinkedList(3, 4, 5, 6, 7, 8)
        val head = createLinkedList(1, 2)
        head!!.next = loopedList
        val nine = Node(9)
        nine.joinToEndOf(loopedList!!)
        nine.next = loopedList
        val actual = detectLoop(head)
        assertEquals(loopedList, actual)
    }

    @Test fun `verify that a runway of 3 returns the fourth node`() {
        val loopedList = createLinkedList(4, 5, 6, 7, 8)
        val head = createLinkedList(1, 2, 3)
        head!!.next = loopedList
        val nine = Node(9)
        nine.joinToEndOf(loopedList!!)
        nine.next = loopedList
        val actual = detectLoop(head)
        assertEquals(loopedList, actual)
    }

    @Test fun `verify that a runway of 4 returns the fifth node`() {
        val loopedList = createLinkedList(5, 6, 7, 8)
        val head = createLinkedList(1, 2, 3, 4)
        head!!.next = loopedList
        val nine = Node(9)
        nine.joinToEndOf(loopedList!!)
        nine.next = loopedList
        val actual = detectLoop(head)
        assertEquals(loopedList, actual)
    }

    @Test fun `verify that a runway of 5 returns the sixth node`() {
        val loopedList = createLinkedList(6, 7, 8)
        val head = createLinkedList(1, 2, 3, 4, 5)
        head!!.next = loopedList
        val nine = Node(9)
        nine.joinToEndOf(loopedList!!)
        nine.next = loopedList
        val actual = detectLoop(head)
        assertEquals(loopedList, actual)
    }

    @Test fun `verify that a runway of 6 returns the seventh node`() {
        val loopedList = createLinkedList(7, 8)
        val head = createLinkedList(1, 2, 3, 4, 5, 6)
        head!!.next = loopedList
        val nine = Node(9)
        nine.joinToEndOf(loopedList!!)
        nine.next = loopedList
        val actual = detectLoop(head)
        assertEquals(loopedList, actual)
    }

    @Test fun `verify that a runway of 7 returns the eighth node`() {
        val loopedList = createLinkedList(8)
        val head = createLinkedList(1, 2, 3, 4, 5, 6, 7)
        head!!.next = loopedList
        val nine = Node(9)
        nine.joinToEndOf(loopedList!!)
        nine.next = loopedList
        val actual = detectLoop(head)
        assertEquals(loopedList, actual)
    }

    @Test fun `verify that a runway of 8 returns the ninth node`() {
        val loopedList = Node(9)
        val head = createLinkedList(1, 2, 3, 4, 5, 6, 7, 8)
        head!!.next = loopedList
        loopedList.joinToEndOf(loopedList)
        val actual = detectLoop(head)
        assertEquals(loopedList, actual)
    }
}

fun <T> Node<T>.joinToEndOf(list: Node<T>) {
    tailrec fun getTail(current: Node<T>): Node<T> {
        val next = current.next ?: return current
        return getTail(next)
    }

    val tail = getTail(list)
    tail.next = this
}
