import com.pajato.ctci.Node

fun <T> detectLoop(list: Node<T>?): Node<T>? {
    tailrec fun getCollisionPoint(slow: Node<T>?, fast: Node<T>?, start: Boolean): Node<T>? {
        if (slow === fast && !start) return slow
        val nextFast = fast?.next?.next ?: return null
        return getCollisionPoint(slow?.next, nextFast, false)
    }
    tailrec fun getLoopNode(slow: Node<T>?, fast: Node<T>?): Node<T>? {
        if (slow === fast) return slow
        return getLoopNode(slow!!.next, fast!!.next)
    }
    val slow = list ?: return null
    val collisionPoint = getCollisionPoint(slow, slow, true) ?: return null

    return getLoopNode(list, collisionPoint)
}
